ARG REPO_ARCH=arm64v8

# production environment
FROM $REPO_ARCH/alpine:3.19

ARG UID=991
ARG GID=991

ENV \
  REPO_ARCH=${REPO_ARCH} \
  UID=${UID} \
  GID=${GID}

RUN \
  addgroup -g "${GID}" app && \
  adduser -D -G app -u "${UID}" -g "" -h /opt/app app && \
  apk add tini

WORKDIR /opt/app
USER app
ENTRYPOINT ["/sbin/tini", "--"]
CMD /bin/sh
